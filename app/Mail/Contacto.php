<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contacto extends Mailable
{
    use Queueable, SerializesModels;
    
    public $post;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($post)
    {
        $this->post = $post;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->post->email, $this->post->nombre)
                ->to('sandracastro@u2.com.co')
                ->bcc(['andresurdaneta@u2.com.co', 'luiscepeda@u2.com.co', 'paulinaramirez@u2.com.co', 'carlosyaruro@u2.com.co'])
                ->view('email');
    }
}
