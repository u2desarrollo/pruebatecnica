Prueba técnica para desarrollador PHP.

Esta prueba consiste en medir sus conocimientos en el desarrollo de aplicaciones haciendo uso de Laravel Framework, consumo de API interna y GIT. Se requiere clonar este proyecto en entorno local e instalar y configurar dicho proyecto.

Ya instalado y configurado, se requiere hacer los siguientes cambios:

- En el formulario de contacto, se necesita cambiar el campo ciudad por dos selects: uno para departamento y otro para ciudad. Las ciudades deberán cargarse dependiendo del departamento seleccionado. Para tal fin se han creado dos servicios REST, los cuales devolverán un json con la información requerida:

Departamentos:
Request GET https://www.tiendaallmarksa.com/api/departamentos
Parámetros: Ninguno.
Response:
{departamentos_cod_depto: int, departamentos_nombre: string}

Ciudades:
Request GET https://www.tiendaallmarksa.com/api/ciudades
Parámetros: departamento => Código del departamento
Response:
{municipios_cod_depto: int, municipios_cod_municipio: int, municipios_nombre: string}

- El select de ciudad deberá trabajar con un buscador en el mismo para que los usuarios puedan buscar ciudades escribiendo una parte del nombre de la ciudad y mostrar los resultados filtrados por esta búsqueda. Puede hacer uso de un plugin para esta funcionalidad.

- BONUS A LA PRUEBA: Actualmente el proyecto tiene funcionalidad de envío de correo. Sin embargo en local no estaría configurado para enviar. ¿Podría configurar el proyecto para que haga envíos de correos desde el entorno local?