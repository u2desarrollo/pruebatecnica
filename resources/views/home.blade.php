<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>U2B2B</title>
  <meta name="keywords" content="U2B2B">
  <meta name="description" content="">
  <meta name="author" content="U2 Software">
  <meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  @include('templates.promogear.css')
  <style type="text/css">
      h5 {
          color: rgb(255, 247, 204);
      }
  </style>
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="one-page">

<div class="preloader"><span class="loader"><span class="loader-inner"></span></span></div>

<div class="main">
  <section
	class="section slide-nav-left banners-section"
	data-background="#1e2d3a"
	data-shadow="rgba(0,0,0,0.02)"
	data-nav-color="#ffffff"
	data-nav-color-text="#1e2d3a"
	data-color="#768088"
	data-title-color="#fff7cc"
	data-title="Inicio"
	data-anchor="inicio">
	<header class="site-header">
	  <div class="container-fluid">
		<div class="row">
		  <div class="col-xs-9 col-sm-4 left-align no-left-indent">
			<div class="btn-group dropdown-item">
			</div>
                        <div class="btn-group dropdown-item share">
			</div>
		  </div>

		  <div class="col-sm-4 logo-box">
			<div class="logo-t">
			  <div class="logo-tr">
				<div class="logo-tc">
				  <a href="#" class="logo">
                                      <img src="{{ asset('templates/promogear/img/U2_B2B_2.png') }}" alt="" style="height: 60px;">
				  </a>
				</div>
			  </div>
			</div>
		  </div>

		  <div class="col-xs-3 col-sm-4 right-align no-right-indent">
			<div class="navbar navbar-default" role="navigation">
			</div>
		  </div>
		</div>
	  </div>

	  <div class="clearfix"></div>
	</header><!-- .site-header -->

	<div class="section-content">
	  <div class="slide slide-one" data-anchor="slide1">
		<div class="image">
		  <img src="{{ asset('templates/promogear/content/img/banner-1-nuevo.png') }}" class="replace-2x" width="906" height="696" alt="">
		</div>

		<div class="section-wrap">
		  <div class="container">
			<div class="row">
			  <div class="col-sm-12 col-sm-offset-1 col-md-6 col-md-offset-6 col-lg-5 col-lg-offset-7 info">
			    <h1 class="section-title">La plataforma de comercio electrónico entre usted y sus distribuidores</h1>
				<h1 class="section-subtitle_por">U2B2B La plataforma que esta revolucionado el mercado colombiano</h1>
				<p class="descriptions"></p>
			  </div><!-- .info -->
			</div>
		  </div>
		</div>
	  </div><!-- .slide -->
	</div><!-- .section-content -->
  </section><!-- .section -->

  

  <section
	class="section"
	data-background="#0667A6"
	data-shadow="rgba(0,0,0,0.03)"
	data-nav-color="#000ff"
	data-nav-color-text="#d73e4d"
	data-color="#768088"
	data-title-color="#fff"
	data-title="Tienda Web"
	data-anchor="tienda-web">
	<div class="section-content">
	  <div class="container section-wrap">
		<h1 class="section-title text-center" data-appear-animation="fadeInDown" data-appear-animation-delay="700">Permita que sus distribuidores realicen sus pedidos</h1>
		<h3 class="section-subtitle text-center" data-appear-animation="fadeInDown" data-appear-animation-delay="700">(Tienda Web Dinámica)</h3>

		<div class="work-carousel carousel-danger" data-appear-animation="fadeInDown" data-appear-animation-delay="700">
		  <div class="row carousel-wrap">
			<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 main-carousel-wrap">
			  <div class="main-carousel">
				<a href="#" id="one" class="work" data-link="works/work-1.html">
				  <div class="image">
					<img src="{{ asset('templates/promogear/img/screenshots/Odonto.png') }}" class="replace-2x" width="600" height="370" alt="">
				  </div>
				</a><!-- .work -->

				<a href="#" id="two" class="work" data-link="works/work-1.html">
				  <div class="image">
					<img src="{{ asset('templates/promogear/img/screenshots/Vinos2.png') }}" class="replace-2x" width="600" height="370" alt="">
				  </div>
				</a><!-- .work -->

				<a href="#" id="three" class="work" data-link="works/work-1.html">
				  <div class="image">
					<img src="{{ asset('templates/promogear/img/screenshots/sport.png') }}" class="replace-2x" width="600" height="370" alt="">
				  </div>
				</a><!-- .work -->

				<a href="#" id="four" class="work" data-link="works/work-1.html">
				  <div class="image">
					<img src="{{ asset('templates/promogear/img/screenshots/clothes.png') }}" class="replace-2x" width="600" height="370" alt="">
				  </div>
				</a><!-- .work -->

			  </div><!-- .main-carousel -->
			</div><!-- .main-carousel-wrap -->

			<div class="second-carousel-wrap">
			  <div class="second-carousel">
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/Vinos2.png') }}" class="replace-2x" width="380" height="370" alt="">
				</div>
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/clothes.png') }}" class="replace-2x" width="380" height="370" alt="">
				</div>
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/sport.png') }}" class="replace-2x" width="380" height="370" alt="">
				</div>
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/odonto.png') }}" class="replace-2x" width="380" height="370" alt="">
				</div>
			  </div>
			</div><!-- .second-carousel-wrap -->

			<div class="second-carousel-wrap right">
			  <div class="second-carousel">
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/sport.png') }}" class="replace-2x" width="380" height="370" alt="">
				</div>
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/clothes.png') }}" class="replace-2x" width="380" height="370" alt="">
				</div>
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/Odonto.png') }}" class="replace-2x" width="380" height="370" alt="">
				</div>
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/Vinos2.png') }}" class="replace-2x" width="380" height="370" alt="">
				</div>
			  </div>
			</div><!-- .second-carousel-wrap -->

			<div class="second-carousel-wrap too">
			  <div class="second-carousel">
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/clothes.png') }}" class="replace-2x" width="240" height="148" alt="">
				</div>
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/Odonto.png') }}" class="replace-2x" width="240" height="148" alt="">
				</div>
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/Vinos2.png') }}" class="replace-2x" width="240" height="148" alt="">
				</div>
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/sport.png') }}" class="replace-2x" width="240" height="148" alt="">
				</div>
			  </div>
			</div><!-- .second-carousel-wrap -->

			<div class="second-carousel-wrap too right">
			  <div class="second-carousel">
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/Vinos2.png') }}" class="replace-2x" width="240" height="148" alt="">
				</div>
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/clothes.png') }}" class="replace-2x" width="240" height="148" alt="">
				</div>
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/Odonto.png') }}" class="replace-2x" width="240" height="148" alt="">
				</div>
				<div class="image">
				  <img src="{{ asset('templates/promogear/img/screenshots/sport.png') }}" class="replace-2x" width="240" height="148" alt="">
				</div>
			  </div>
			</div><!-- .second-carousel-wrap -->
		  </div><!-- .carousel-wrap -->

		  <div class="clearfix"></div>

		  <div class="thumbs-wrap">
			<div class="thumbs">
			  <a href="#one" class="selected">
				<img src="{{ asset('templates/promogear/img/screenshots/Odonto.png') }}" class="replace-2x" width="90" height="60" alt="">
			  </a>
			  <a href="#two">
				<img src="{{ asset('templates/promogear/img/screenshots/Vinos2.png') }}" class="replace-2x" width="90" height="60" alt="">
			  </a>
			  <a href="#three">
				<img src="{{ asset('templates/promogear/img/screenshots/sport.png') }}" class="replace-2x" width="90" height="60" alt="">
			  </a>
			  <a href="#four">
				<img src="{{ asset('templates/promogear/img/screenshots/clothes.png') }}" class="replace-2x" width="90" height="60" alt="">
			  </a>
			</div><!-- .thumbs -->

			<div class="navigation">
			  <a class="prev" href="#"><i class="fa fa-angle-left"></i></a>
			  <a class="next" href="#"><i class="fa fa-angle-right"></i></a>
			</div>
		  </div><!-- .thumbs-wrap -->
		</div><!-- .work-carousel -->
		<div class="row">
        	
        	<div class="col-md-4 col-sm-12" >
        		<ul class="list-mobile">
					<li><h5>Autenticación (Login).</h5></li>
					<li><h5>Precios según categoría de cliente.</h5></li>
					<li><h5>Promociones por categoría.</h5></li>
				</ul>
	       	</div>
	       	<div class="col-md-4 col-sm-12" >
        		<ul class="list-mobile">
					
					<li><h5>Control cupos de crédito.</h5></li>
					<li><h5>Consulta cartera.</h5></li>
					<li><h5>100% personalizada.</h5></li>
				</ul>
	       	</div>
	       	<div class="col-md-4 col-sm-12" >
        		<ul class="list-mobile">
					<li><h5>Recaudos en línea a través de:</h5></li>
				</ul>
				<div class="col-md-4 col-sm-4">
				<img src="{{ asset('templates/promogear/img/screenshots/pse.png') }}" width="100" height="120" alt="">
				</div>
				<div class="col-md-4  col-sm-4">
				<img src="{{ asset('templates/promogear/img/screenshots/via-baloto.png') }}" width="100" height="120" alt="">
				</div>
				<div class="col-md-4  col-sm-4">
				<img src="{{ asset('templates/promogear/img/screenshots/logo-efecty.png') }}" width="100" height="120" alt="">
				</div>
				
	       	</div>
        </div>  
	  </div>
	</div><!-- .section-content -->
  </section><!-- .section -->

  <section
	class="section promo-4-section"
	data-background="#1e2d3a"
	data-shadow="rgba(0,0,0,0.03)"
	data-nav-color="#ffffff"
	data-nav-color-text="#d73e4d"
	data-color="#768088"
	data-title-color="#fff"
	data-title="Tienda APP"
	data-anchor="tienda-desde-smartphone">
	<div class="section-content not-scroll">
	  <div class="container section-wrap top-padding">
		<div class="promo-box text-center inverse">
		  <div class="features-text">
		  	<h1 class="section-title">APP Para sus distribuidores</h1>
			<h1 class="section-subtitle">(Compatibilidad con cualquier dispositivo)</h1>
		  </div><!-- .features-text -->

		  <div class="promo-feature">
			<div class="image">
			  <img src="{{ asset('templates/promogear/content/img/feature-phone-nuevo.png') }}" class="replace-2x" width="551" height="702" alt="">

			  <div class="bullet left bullet-5" data-appear-animation="fadeInLeft" data-appear-animation-delay="400">
				<div class="number">1</div>
				<div class="line">
				  <div class="bullet-content">
					<a href="#" class="close">
					  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
						<path fill="#fff7cc" d="M23.3,23.3c-0.6,0.6-1.5,0.6-2.1,0L0.7,2.8c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0
						l20.5,20.5C23.9,21.8,23.9,22.7,23.3,23.3L23.3,23.3z M23.3,2.8L2.8,23.3c-0.6,0.6-1.5,0.6-2.1,0c-0.6-0.6-0.6-1.5,0-2.1L21.2,0.7
						c0.6-0.6,1.5-0.6,2.1,0C23.9,1.3,23.9,2.2,23.3,2.8L23.3,2.8z"></path>
					  </svg>
					</a>
					<h5 class="title">Búsqueda ultra rápida.</h5>
					<p>Búsquedas elásticas por descripción de producto.</p>
				  </div>
				</div>
			  </div><!-- .bullet -->

			  <div class="bullet bullet-6" data-appear-animation="fadeInLeft" data-appear-animation-delay="600">
				<div class="number">2</div>
				<div class="line">
				  <div class="bullet-content">
					<a href="#" class="close">
					  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
						<path fill="#fff7cc" d="M23.3,23.3c-0.6,0.6-1.5,0.6-2.1,0L0.7,2.8c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0
						l20.5,20.5C23.9,21.8,23.9,22.7,23.3,23.3L23.3,23.3z M23.3,2.8L2.8,23.3c-0.6,0.6-1.5,0.6-2.1,0c-0.6-0.6-0.6-1.5,0-2.1L21.2,0.7
						c0.6-0.6,1.5-0.6,2.1,0C23.9,1.3,23.9,2.2,23.3,2.8L23.3,2.8z"></path>
					  </svg>
					</a>
					<h5 class="title">Asistente de reposición.</h5>
					<p>Sus clientes podrán consultar las últimas compras y sugeridos de reposición.</p>
				  </div>
				</div>
			  </div><!-- .bullet -->

			  <div class="bullet left bullet-7" data-appear-animation="fadeInLeft" data-appear-animation-delay="800">
				<div class="number">3</div>
				<div class="line">
				  <div class="bullet-content">
					<a href="#" class="close">
					  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
						<path fill="#fff7cc" d="M23.3,23.3c-0.6,0.6-1.5,0.6-2.1,0L0.7,2.8c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0
						l20.5,20.5C23.9,21.8,23.9,22.7,23.3,23.3L23.3,23.3z M23.3,2.8L2.8,23.3c-0.6,0.6-1.5,0.6-2.1,0c-0.6-0.6-0.6-1.5,0-2.1L21.2,0.7
						c0.6-0.6,1.5-0.6,2.1,0C23.9,1.3,23.9,2.2,23.3,2.8L23.3,2.8z"></path>
					  </svg>
					</a>
					<h5 class="title">Navegabilidad.</h5>
					<p>Recorra la APP de forma fácil e intuitiva.</p>
				  </div>
				</div>
			  </div><!-- .bullet -->
			</div>
		  </div><!-- .promo-feature -->

		  	<ul class="list-mobile mobile">
				<li>
					<h5>Búsqueda ultra rápida.</h5>
					<p>Búsquedas elásticas por descripción de producto.</p>
				</li>
				<li>
					<h5>Asistente de reposición.</h5>
					<p>Sus clientes podrán consultar las últimas compras y sugeridos de reposición.</p>
				</li>
				<li>
					<h5>Navegabilidad.</h5>
					<p>Recorra la APP de forma fácil e intuitiva.</p>
				</li>
			</ul>
		</div><!-- .promo-box -->
	  </div>
	</div><!-- .section-content -->
  </section><!-- .section -->

  <section
	class="section call-action-section"
	data-background="#0667A6"
	data-shadow="rgba(0,0,0,0.03)"
	data-nav-color="#ffffff"
	data-nav-color-text="#d73e4d"
	data-color="#768088"
	data-title-color="#fff"
	data-title="Campañas"
	data-anchor="campanas">
	<div class="section-content not-scroll">
	  <div class="container section-wrap">
      	<div class="promo-box text-center inverse">
		  <div class="features-text">
			<h1 class="section-title" data-appear-animation="fadeInDown" data-appear-animation-delay="700">ADMINISTRACIÓN DE CAMPAÑAS</h1>
			<h1 class="section-subtitle" data-appear-animation="fadeInDown" data-appear-animation-delay="700">(Difusión vía WathSapp, Mailing, SMS, Notificaciones APP)</h1>
		  </div><!-- .features-text -->

		  <div>
			<div class="col-md-7 col-sm-12" >
                            <img src="{{ asset('templates/promogear/content/img/difusion.png') }}" class="replace-2x" alt="" data-appear-animation="fadeInDown" data-appear-animation-delay="700" style="margin-left: 50px;">
			</div>
			<div class="col-md-5 col-sm-12" style="padding: 70px 0">
				<ul class="list-mobile">
					<!--li><h5>Envió de notificaciones por todos los medio disponibles.</h5></li-->
					<li><h5>Segmente sus clientes.</h5></li>
					<li><h5>Defina precios por categoria.</h5></li>
					<li><h5>Diseñe campañas.</h5></li>
					<li><h5>Fije promociones.</h5></li>
					<li><h5>Determine objetivos.</h5></li>
					<li><h5>Mida resultados.</h5></li>
					<!--li><h5>Difunda promociones(WathSapp, Mailing, SMS, Notificaciones APP).</h5></li-->
				</ul>
        		
        	</div>
		  </div><!-- .promo-feature -->
      	</div><!-- .promo-box -->
	  </div>
	</div><!-- .section-content -->
  </section><!-- .section -->


  <section
	class="section call-action-section"
	data-background="#1e2d3a"
	data-shadow="rgba(0,0,0,0.03)"
	data-nav-color="#ffffff"
	data-nav-color-text="#d73e4d"
	data-color="#768088"
	data-title-color="#fff"
	data-title="Callcenter"
	data-anchor="callcenter">
	<div class="section-content not-scroll">
	  <div class="container section-wrap">
              <div class="promo-box text-center inverse">
		  <div class="features-text">
			<h1 class="section-title" data-appear-animation="fadeInDown" data-appear-animation-delay="700">REALICE SEGUIMIENTOS A SUS CAMPAÑAS</h1>
			<h1 class="section-subtitle" data-appear-animation="fadeInDown" data-appear-animation-delay="700">(MÓDULO DE CALLCENTER INTEGRADO)</h1>
		  </div><!-- .features-text -->

		  <div >
			<div class="col-md-8 col-sm-12">
                <img src="{{ asset('templates/promogear/content/img/callcenter.png') }}" width="1000" height="500" class="replace-2x" alt="">
            </div>
            <div class="col-md-4 col-sm-12" style="padding: 70px 30px">
				<ul class="list-mobile">
					<li><h5>Gestión y seguimiento de campañas. </h5></li>
					<li><h5>Refuerzo de promociones. </h5></li>
					<li><h5>Gestión de cobranza. </h5></li>
					<li><h5>Reducción de costos(Fuerza de ventas). </h5></li>
					<li><h5>Indicadores Efectividad. </h5></li>
				</ul>
        	</div>
    		   <!--div class="bullet left bullet-8" data-appear-animation="fadeInLeft" data-appear-animation-delay="400">
				<div class="number">1</div>
				<div class="line">
				  <div class="bullet-content">
					<a href="#" class="close">
					  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
						<path fill="#fff7cc" d="M23.3,23.3c-0.6,0.6-1.5,0.6-2.1,0L0.7,2.8c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0
						l20.5,20.5C23.9,21.8,23.9,22.7,23.3,23.3L23.3,23.3z M23.3,2.8L2.8,23.3c-0.6,0.6-1.5,0.6-2.1,0c-0.6-0.6-0.6-1.5,0-2.1L21.2,0.7
						c0.6-0.6,1.5-0.6,2.1,0C23.9,1.3,23.9,2.2,23.3,2.8L23.3,2.8z"></path>
					  </svg>
					</a>
					<h5 class="title">Integración</h5>
					<p>Gracias a su integración con el B2B, podrá asistir a sus clientes sin cambiar de entorno</p>
				  </div>
				</div>
			  </div><!-- .bullet -->

			  <!--div class="bullet bullet-9" data-appear-animation="fadeInLeft" data-appear-animation-delay="600">
				<div class="number">2</div>
				<div class="line">
				  <div class="bullet-content">
					<a href="#" class="close">
					  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
						<path fill="#fff7cc" d="M23.3,23.3c-0.6,0.6-1.5,0.6-2.1,0L0.7,2.8c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0
						l20.5,20.5C23.9,21.8,23.9,22.7,23.3,23.3L23.3,23.3z M23.3,2.8L2.8,23.3c-0.6,0.6-1.5,0.6-2.1,0c-0.6-0.6-0.6-1.5,0-2.1L21.2,0.7
						c0.6-0.6,1.5-0.6,2.1,0C23.9,1.3,23.9,2.2,23.3,2.8L23.3,2.8z"></path>
					  </svg>
					</a>
					<h5 class="title">Estado de servidor de llamadas</h5>
					<p>Obtenga información en tiempo real del estado de su servidor de llamadas</p>
				  </div>
				</div>
			  </div><!-- .bullet -->

			  <!--div class="bullet left bullet-10" data-appear-animation="fadeInLeft" data-appear-animation-delay="800">
				<div class="number">3</div>
				<div class="line">
				  <div class="bullet-content">
					<a href="#" class="close">
					  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
						<path fill="#fff7cc" d="M23.3,23.3c-0.6,0.6-1.5,0.6-2.1,0L0.7,2.8c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0
						l20.5,20.5C23.9,21.8,23.9,22.7,23.3,23.3L23.3,23.3z M23.3,2.8L2.8,23.3c-0.6,0.6-1.5,0.6-2.1,0c-0.6-0.6-0.6-1.5,0-2.1L21.2,0.7
						c0.6-0.6,1.5-0.6,2.1,0C23.9,1.3,23.9,2.2,23.3,2.8L23.3,2.8z"></path>
					  </svg>
					</a>
					<h5 class="title">Llamadas</h5>
					<p>Observe de forma gráfica las llamadas gestionadas</p>
				  </div>
				</div>
			  </div><!-- .bullet -->
			
		  </div><!-- .promo-feature -->
              </div><!-- .promo-box -->
	  </div>
	</div><!-- .section-content -->
  </section><!-- .section -->

  <section
	class="section call-action-section"
	data-background="#0667A6"
	data-shadow="rgba(0,0,0,0.03)"
	data-nav-color="#ffffff"
	data-nav-color-text="#d73e4d"
	data-color="#768088"
	data-title-color="#fff"
	data-title="Seguimiento a vendedores"
	data-anchor="seguimiento-vendedores">
	<div class="section-content not-scroll">
	  <div class="container section-wrap">
              <div class="promo-box text-center inverse">
		  <div class="features-text">
			<h1 class="section-title" data-appear-animation="fadeInDown" data-appear-animation-delay="700">REALICE CONTROL Y SEGUIMIENTO A VENDEDORES</h1>
			<h1 class="section-subtitle" data-appear-animation="fadeInDown" data-appear-animation-delay="700">(APP PARA FUERZA DE VENTAS)</h1>
		  </div><!-- .features-text -->

		  <div class="promo-feature">
			<div class="col-md-7 col-sm-12" >
                            <img src="{{ asset('templates/promogear/content/img/banner-2-nuevo.png') }}" class="replace-2x" alt="" data-appear-animation="fadeInDown" data-appear-animation-delay="700" style="margin-left: 0px;">
			</div>
			<div class="col-md-5 col-sm-12" style="padding: 70px 0">
				<ul class="list-mobile">
					<li><h5>Seguimiento satelital.</h5></li>
					<li><h5>Registro de pedidos en línea.</h5></li>
					<li><h5>Control y validación de gastos.</h5></li>
					<li><h5>Consulta de existencias.</h5></li>
					<li><h5>Captación de nuevos clientes.</h5></li>
					<li><h5>Penetración a nuevos mercados.</h5></li>     
				</ul>
        		
        	</div>
		  </div><!-- .promo-feature -->
              </div><!-- .promo-box -->
	  </div>
	</div><!-- .section-content -->
  </section><!-- .section -->

  
  <!--section
	class="section call-action-section"
	data-background="#0667A6"
	data-shadow="rgba(0,0,0,0.03)"
	data-nav-color="#ffffff"
	data-nav-color-text="#d73e4d"
	data-color="#768088"
	data-title-color="#fff"
	data-title="Analytics (BI)"
	data-anchor="analytics">
	<div class="section-content not-scroll">
	  <div class="container section-wrap">
              <div class="promo-box text-center inverse">
		  <div class="features-text">
			<h1 class="section-title" data-appear-animation="fadeInDown" data-appear-animation-delay="700">ANALYTICS (BI)</h1>
		  </div>

		  <div class="promo-feature">
			<div class="image">
                            <img src="{{ asset('templates/promogear/content/img/analytics.png') }}" class="replace-2x" alt="" data-appear-animation="fadeInDown" data-appear-animation-delay="700" style="margin-left: 0px;">
			</div>
		  </div>
              </div>
	  </div>
	</div>
  </section><!-- .section -->

  <section
	class="section call-action-section"
	data-background="#1e2d3a"
	data-shadow="rgba(0,0,0,0.03)"
	data-nav-color="#ffffff"
	data-nav-color-text="#d73e4d"
	data-color="#768088"
	data-title-color="#fff"
	data-title="Conectividad ERP"
	data-anchor="conectividad-erp">
	<div class="section-content not-scroll">
	  <div class="container section-wrap">
              <div class="promo-box text-center inverse">
		  <div class="features-text">
			<h1 class="section-title" data-appear-animation="fadeInDown" data-appear-animation-delay="700">CONECTIVIDAD ERP</h1>
			<h1 class="section-subtitle" data-appear-animation="fadeInDown" data-appear-animation-delay="700">(FACIL Y RAPIDA INTEGRACIÓN)</h1>
		  </div><!-- .features-text -->

		 <div class="col-md-7 col-sm-12">
                            <img src="{{ asset('templates/promogear/content/img/conectividad-erp.png') }}" class="replace-2x" alt="" data-appear-animation="fadeInDown" data-appear-animation-delay="700" style="margin-left: 0px">
			</div>
			<div class="col-md-5 col-sm-12" style="padding: 70px 30px">
				<ul class="list-mobile">
					<li><h5>Web Services. </h5></li>
					<li><h5>API.</h5></li>
					<li><h5>Archivos excel. </h5></li>
				</ul>
        		
        	</div>
              </div><!-- .promo-box -->
	  </div>
	</div><!-- .section-content -->
  </section><!-- .section -->

  <section
	class="section promo-3-section"
	data-background="#0667A6"
	data-shadow="rgba(0,0,0,0.03)"
	data-nav-color="#ffffff"
	data-nav-color-text="#d73e4d"
	data-color="#768088"
	data-title-color="#fff"
	data-title="Tablero de instrumentos"
	data-anchor="dashboard">
      <div class="section-content not-scroll" style="max-height: inherit;">
	  <div class="container section-wrap top-padding">
		<div class="promo-box text-center inverse">
		  <div class="features-text">
			<h1 class="section-title" data-appear-animation="fadeInDown" data-appear-animation-delay="700">MONITOREE SUS VENTAS A TRAVÉS DEL MODULO ADMINISTRATIVO</h1>
		  </div><!-- .features-text -->

		  <div class="promo-feature">
			<div class="image">
                            <img src="{{ asset('templates/promogear/content/img/dashboard.png') }}" class="replace-2x" width="551" height="702" alt="" style="margin-left: 0px;">
                            <div class="bullet bullet-1" data-appear-animation="fadeInLeft" data-appear-animation-delay="600">
                                    <div class="number">1</div>
                                    <div class="line">
                                      <div class="bullet-content">
                                            <a href="#" class="close">
                                              <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                    <path fill="#fff7cc" d="M23.3,23.3c-0.6,0.6-1.5,0.6-2.1,0L0.7,2.8c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0
                                                    l20.5,20.5C23.9,21.8,23.9,22.7,23.3,23.3L23.3,23.3z M23.3,2.8L2.8,23.3c-0.6,0.6-1.5,0.6-2.1,0c-0.6-0.6-0.6-1.5,0-2.1L21.2,0.7
                                                    c0.6-0.6,1.5-0.6,2.1,0C23.9,1.3,23.9,2.2,23.3,2.8L23.3,2.8z"></path>
                                              </svg>
                                            </a>
                                            <h5 class="title">NOTIFICACIONES</h5>
                                            <p>Pedidos abandonados, Clientes en mora, Envio de mensajes</p>
                                      </div>
                                    </div>
                            </div><!-- .bullet -->
                            <div class="bullet left bullet-2" data-appear-animation="fadeInLeft" data-appear-animation-delay="400">
                                  <div class="number">2</div>
                                  <div class="line">
                                      <div class="bullet-content">
                                          <a href="#" class="close">
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                  <path fill="#fff7cc" d="M23.3,23.3c-0.6,0.6-1.5,0.6-2.1,0L0.7,2.8c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0
                                                  l20.5,20.5C23.9,21.8,23.9,22.7,23.3,23.3L23.3,23.3z M23.3,2.8L2.8,23.3c-0.6,0.6-1.5,0.6-2.1,0c-0.6-0.6-0.6-1.5,0-2.1L21.2,0.7
                                                  c0.6-0.6,1.5-0.6,2.1,0C23.9,1.3,23.9,2.2,23.3,2.8L23.3,2.8z"></path>
                                            </svg>
                                          </a>
                                          <h5 class="title">INFORMACIÓN DESTACADA</h5>
                                          <p>Nuevas ventas, Nuevos pedidos, Nuevos clientes, Porcentaje de cumplimiento</p>
                                    </div>
                                  </div>
                            </div><!-- .bullet -->



                          <div class="bullet bullet-3" data-appear-animation="fadeInLeft" data-appear-animation-delay="800">
				<div class="number">3</div>
				<div class="line">
				  <div class="bullet-content">
					<a href="#" class="close">
					  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
						<path fill="#fff7cc" d="M23.3,23.3c-0.6,0.6-1.5,0.6-2.1,0L0.7,2.8c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0
						l20.5,20.5C23.9,21.8,23.9,22.7,23.3,23.3L23.3,23.3z M23.3,2.8L2.8,23.3c-0.6,0.6-1.5,0.6-2.1,0c-0.6-0.6-0.6-1.5,0-2.1L21.2,0.7
						c0.6-0.6,1.5-0.6,2.1,0C23.9,1.3,23.9,2.2,23.3,2.8L23.3,2.8z"></path>
					  </svg>
					</a>
					<h5 class="title">GRÁFICOS VENTAS</h5>
					<p>Manejo de Analitics, Compare sus ventas VS presupuesto</p>
				  </div>
				</div>
			  </div><!-- .bullet -->

			  <div class="bullet left bullet-4" data-appear-animation="fadeInLeft" data-appear-animation-delay="1200">
				<div class="number">4</div>
				<div class="line">
				  <div class="bullet-content">
					<a href="#" class="close">
					  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
						<path fill="#fff7cc" d="M23.3,23.3c-0.6,0.6-1.5,0.6-2.1,0L0.7,2.8c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0
						l20.5,20.5C23.9,21.8,23.9,22.7,23.3,23.3L23.3,23.3z M23.3,2.8L2.8,23.3c-0.6,0.6-1.5,0.6-2.1,0c-0.6-0.6-0.6-1.5,0-2.1L21.2,0.7
						c0.6-0.6,1.5-0.6,2.1,0C23.9,1.3,23.9,2.2,23.3,2.8L23.3,2.8z"></path>
					  </svg>
					</a>
					<h5 class="title">VISITANTES</h5>
					<p>Mida la concurrencia de su tienda en tiempo real</p>
				  </div>
				</div>
			  </div><!-- .bullet -->
			</div>
		  </div><!-- .promo-feature -->

			<ul class="list-mobile mobile">
				<li>
					<h5>NOTIFICACIONES</h5>
					<p>Pedidos abandonados, Clientes en mora, Envio de mensajes</p>
				</li>
				<li>
					<h5>INFORMACIÓN DESTACADA</h5>
					<p>Nuevas ventas, Nuevos pedidos, Nuevos clientes, % de Cumplimiento</p>
				</li>
				<li>
					<h5>GRÁFICOS VENTAS</h5>
					<p>Manejo de Analitics, Compare sus ventas VS presupuesto</p>
				</li>
				<li>
					<h5>VISITANTES</h5>
					<p>Mida la concurrencia de su tienda en tiempo real</p>
				</li>
			</ul>

		</div><!-- .promo-box -->
	  </div>
	</div><!-- .section-content -->
  </section><!-- .section -->
  <section
	class="section align-without-title services-section"
	data-background="#1e2d3a"
	data-shadow="rgba(0,0,0,0.03)"
	data-nav-color="#ffffff"
	data-nav-color-text="#d73e4d"
	data-color="#768088"
	data-title-color="#fff"
	data-title="¿En qué consiste U2B2B?"
	data-anchor="b2b">
	<div class="section-content">
	  <div class="container section-wrap">
		<h1 class="section-title text-center" data-appear-animation="fadeInDown" data-appear-animation-delay="900">U2B2B La plataforma que esta revolucionado el mercado colombiano </h1>

		<div class="services-wrap inverse no-appear">
		  <div class="wrap-container">
			<div class="column column-one">
			  <div class="circle-wrap" data-appear-animation="zoomIn" data-appear-animation-delay="600">
				<canvas class="circle-skill" data-color="#1eb3c5" data-percent="75" data-title="Administracion Campañas" width="150" height="150"></canvas>
				<div class="description">
				  <h3 class="title">Administración Campañas</h3>
				  <div class="text">
				  	<p style="margin-bottom: 8px">Segmente a sus clientes.</p>
					<p style="margin-bottom: 8px">Defina precios por categoria.</p>
					<p style="margin-bottom: 8px">Diseñe campañas y promociones.</p>
					<p style="margin-bottom: 8px">Determine objetivos.</p>
					<p style="margin-bottom: 8px">Mida los resultados.</p>
					<p style="margin-bottom: 8px">Difunda promociones (WathSapp, Mailing, SMS, Notificaciones APP).</p>
				  </div>
				</div>
			  </div>
			</div><!-- .column-one -->

			<div class="column column-two">
			  <div class="circle-wrap" data-appear-animation="zoomIn" data-appear-animation-delay="400">
				<canvas class="circle-skill" data-color="#25b785" data-percent="60" data-title="Call Center" width="150" height="150"></canvas>
				<div class="description">
				  <h3 class="title">Call Center</h3>
				  <div class="text">
					<p style="margin-bottom: 8px">Gestión y seguimiento de campañas.</p>
					<p style="margin-bottom: 8px">Refuerzo de promociones.</p>
					<p style="margin-bottom: 8px">Gestión de cobranza.</p>
					<p style="margin-bottom: 8px">Reducción de costos(Fuerza de ventas).</p>
					<p style="margin-bottom: 8px">Medición Efectividad.</p>
				  </div>
				</div>
			  </div>

			  <div class="circle-wrap" data-appear-animation="zoomIn" data-appear-animation-delay="500">
				<canvas class="circle-skill" data-color="#4e67c4" data-percent="65" data-title="Seguimiento Fuerza ventas" width="150" height="150"></canvas>
				<div class="description">
				  <h3 class="title">Seguimiento Fuerza ventas</h3>
				  <div class="text">
					<p style="margin-bottom: 8px">Seguimiento Satelital</p>
					<p style="margin-bottom: 8px">Control y validación de gastos.</p>
					<p style="margin-bottom: 8px">Registro de pedidos.</p>
					<p style="margin-bottom: 8px">Consulta de existencias.</p>
					<p style="margin-bottom: 8px">Captación de nuevos clientes.</p>
					<p style="margin-bottom: 8px">Penetración nuevos mercados.</p>
				  </div>
				</div>
			  </div>
			</div><!-- .column-two -->

			<div class="column column-three column-center">
			  <div class="circle-wrap main-circle" data-appear-animation="zoomIn" data-appear-animation-delay="100">
                              <div class="circle-content" style="padding-left: 75px;">
				  <h3 class="title">U2B2B</h3>
				</div>

				<div class="opened-content">
				  <a href="#" class="close">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
					  <path fill="inherit" d="M23.3,23.3c-0.6,0.6-1.5,0.6-2.1,0L0.7,2.8c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0
					  l20.5,20.5C23.9,21.8,23.9,22.7,23.3,23.3L23.3,23.3z M23.3,2.8L2.8,23.3c-0.6,0.6-1.5,0.6-2.1,0c-0.6-0.6-0.6-1.5,0-2.1L21.2,0.7
					  c0.6-0.6,1.5-0.6,2.1,0C23.9,1.3,23.9,2.2,23.3,2.8L23.3,2.8z"/>
					</svg>
				  </a>
				</div><!-- .opened-content -->
			  </div><!-- .main-circle -->
			</div><!-- .column-three -->

			<div class="column column-four">
			  <div class="circle-wrap" data-appear-animation="zoomIn" data-appear-animation-delay="700">
				<canvas class="circle-skill" data-color="#da8326" data-percent="90" data-title="Analytics (BI)" width="150" height="150"></canvas>
				<div class="description">
				  <h3 class="title">Analytics (BI)</h3>
				  <div class="text">
					<p style="margin-bottom: 8px">Graficas Estadísticas (Gerencia).</p>
					<p style="margin-bottom: 8px">Análisis de Ventas y Utilidades (Clientes, Líneas de Producto, Vendedores).</p>
				  </div>
				</div>
			  </div>

			  <div class="circle-wrap" data-appear-animation="zoomIn" data-appear-animation-delay="800">
				<canvas class="circle-skill" data-color="#8f52c8" data-percent="60" data-title="Conectividad con su ERP" width="150" height="150"></canvas>
				<div class="description">
				  <h3 class="title">Conectividad con su ERP</h3>
				  <div class="text">
					<p style="margin-bottom: 8px">Fácil conexión Web Servicies, API, Archivos Excel (Existencias, Clientes, Cartera).</p>
					<p style="margin-bottom: 8px">Facturación Electrónica (Opcional) </p>
				  </div>
				</div>
			  </div>
			</div><!-- .column-four -->

			<div class="column column-five">
			  <div class="circle-wrap" data-appear-animation="zoomIn" data-appear-animation-delay="700">
				<canvas class="circle-skill" data-color="#da8326" data-percent="90" data-title="Su tienda Web" width="150" height="150"></canvas>
				<div class="description">
				  <h3 class="title">Su Tienda Web</h3>
				  <div class="text">
					<p style="margin-bottom: 8px">Tienda web dinámica.</p>
					<p style="margin-bottom: 8px">Precios según Categoría de cliente.</p>
					<p style="margin-bottom: 8px">Control cupos de crédito.</p>
					<p style="margin-bottom: 8px">Consulta cartera.</p>
					<p style="margin-bottom: 8px">Pagos en línea (PSE, Efecty y puntos Baloto) .</p>
					<p style="margin-bottom: 8px">100% personalizada.</p>
				  </div>
				</div>
			  </div>
			</div><!-- .column-five -->
		  </div><!-- .wrap-container -->
		  	<ul class="list-mobile mobile">
				<li>
					<h5>Administración Campañas</h5>
					<p style="margin-bottom: 8px">Segmente a sus clientes.</p>
					<p style="margin-bottom: 8px">Defina precios por categoria.</p>
					<p style="margin-bottom: 8px">Diseñe campañas y promociones.</p>
					<p style="margin-bottom: 8px">Determine objetivos.</p>
					<p style="margin-bottom: 8px">Mida los resultados.</p>
					<p style="margin-bottom: 8px">Difunda promociones(WathSapp, Mailing, SMS, Notificaciones APP).</p>
				</li>
				<li>
					<h5>Call Center</h5>
					<p style="margin-bottom: 8px">Gestión y seguimiento de campañas.</p>
					<p style="margin-bottom: 8px">Refuerzo de promociones.</p>
					<p style="margin-bottom: 8px">Gestión de cobranza.</p>
					<p style="margin-bottom: 8px">Reducción de costos(Fuerza de ventas).</p>
					<p style="margin-bottom: 8px">Medición Efectividad.</p>
				</li>
				<li>
					<h5>Seguimiento Fuerza ventas</h5>
					<p style="margin-bottom: 8px">Seguimiento Satelital</p>
					<p style="margin-bottom: 8px">Control y validación de gastos.</p>
					<p style="margin-bottom: 8px">Registro de pedidos.</p>
					<p style="margin-bottom: 8px">Consulta de existencias.</p>
					<p style="margin-bottom: 8px">Captación de nuevos clientes.</p>
					<p style="margin-bottom: 8px">Penetración nuevos mercados.</p>
				</li>
				<li>
					<h5>Analytics (BI)</h5>
					<p style="margin-bottom: 8px">Graficas Estadísticas (Gerencia).</p>
					<p style="margin-bottom: 8px">Análisis de Ventas y Utilidades (Clientes, Líneas de Producto, Vendedores).</p>
				</li>
				<li>
					<h5>Conectividad con su ERP</h5>
					<p style="margin-bottom: 8px">Fácil conexión Web Servicies, API, Archivos Excel (Existencias, Clientes, Cartera).</p>
					<p style="margin-bottom: 8px">Facturación Electrónica (Opcional) </p>
				</li>
				<li>
					<h5>Su Tienda Web</h5>
					<p style="margin-bottom: 8px">Tienda web dinámica.</p>
					<p style="margin-bottom: 8px">Precios según Categoría de cliente.</p>
					<p style="margin-bottom: 8px">Control cupos de crédito.</p>
					<p style="margin-bottom: 8px">Consulta cartera.</p>
					<p style="margin-bottom: 8px">Pagos en línea (PSE, Efecty y puntos Baloto) .</p>
					<p style="margin-bottom: 8px">100% personalizada.</p>
				</li>
			</ul>
		</div><!-- .services-wrap -->
	  </div>
	</div><!-- .section-content -->
  </section><!-- .section -->



  <section
	class="section contact-section"
	data-background="#0667A6"
	data-shadow="rgba(0,0,0,0.03)"
	data-nav-color="#ffffff"
	data-nav-color-text="#d73e4d"
	data-color="#768088"
	data-title-color="#fff"
	data-title="Contacto"
	data-anchor="contacto">
	<div class="section-content">
	  <div class="container section-wrap">
		<h1 class="section-title" data-appear-animation="fadeInLeft" data-appear-animation-delay="100">CONTACTO</h1>

		<div class="row">
		  <div class="col-sm-6">
			<div class="contact-info" data-appear-animation="fadeInLeft" data-appear-animation-delay="300">
			  <div class="icon"><i class="fa fa-mobile"></i></div>
			  <h4 class="title">Teléfono:</h4>
			  <div class="text">(571) 6202821</div>
			</div><!-- .contact-info -->

			<div class="contact-info" data-appear-animation="fadeInLeft" data-appear-animation-delay="400">
			  <div class="icon"><i class="fa fa-envelope-o"></i></div>
			  <h4 class="title">Email:</h4>
			  <div class="text"><a href="mailto:ventas@u2.com.co">ventas@u2.com.co</a></div>
			</div><!-- .contact-info -->

			<div class="contact-info" data-appear-animation="fadeInLeft" data-appear-animation-delay="500">
			  <div class="icon"><i class="fa fa-map-marker"></i></div>
			  <h4 class="title">Dirección:</h4>
			  <address class="text">Carrera 15 # 122-35. Torre 2. Oficina 702.</address>
			</div><!-- .contact-info -->
		  </div>

		  <div class="col-sm-6">
                      @if(session()->has('mensaje'))
                      <div class="alert alert-info">
                          <h5>{{ session()->get('mensaje') }}</h5>
                      </div>
                      @endif
                      <form role="form" class="contact-form inverse" action="{{ route('enviar-email') }}" method="post">
                          {!! csrf_field() !!}
			  <h3 class="form-title" data-appear-animation="fadeInRight" data-appear-animation-delay="300">Déjenos un mensaje</h3>

			  	<div class="form-group name" data-appear-animation="fadeInRight" data-appear-animation-delay="400">
					<input type="text" class="form-control" name="nombre" placeholder="Nombre *">
			  	</div>

              	<div class="form-group" data-appear-animation="fadeInRight" data-appear-animation-delay="500">
					<input type="number" class="form-control" name="telefono" placeholder="Teléfono *">
			 	 </div>

              	<div class="form-group email" data-appear-animation="fadeInRight" data-appear-animation-delay="500">
					<input type="email" class="form-control" name="email" placeholder="Email *">
			  	</div>

			  	<div class="form-group" data-appear-animation="fadeInRight" data-appear-animation-delay="400">
					<input type="text" class="form-control" name="empresa" placeholder="Empresa *">
			  	</div>

			  	<div class="form-group" data-appear-animation="fadeInRight" data-appear-animation-delay="400">
					<input type="text" class="form-control" name="actividad-empresa" placeholder="Actividad de la empresa *">
			  	</div>

			  	<div class="form-group" data-appear-animation="fadeInRight" data-appear-animation-delay="400">
					<input type="text" class="form-control" name="ciudad" placeholder="Ciudad *">
			  	</div>

			  	<div class="form-group comment" data-appear-animation="fadeInRight" data-appear-animation-delay="600">
					<textarea  class="form-control" name="mensaje" placeholder="Su mensaje *"></textarea>
			  	</div>

			  <button type="submit" class="btn btn-default submit-btn" data-appear-animation="fadeInRight" data-appear-animation-delay="700">Enviar</button>
			  <span class="form-message animated fadeInRight" data-appear-animation="fadeInRight" data-appear-animation-delay="700"></span>
			</form><!-- .contact-form -->
		  </div>
		</div>
	  </div>
	</div><!-- .section-content -->

	<footer class="site-footer">
	  <div class="container">
		<div class="row">
		  <div class="col-sm-4 left-align">
			<div class="copyright">U2 Software {{ date('Y') }}</div>
		  </div>

		  <div class="col-sm-4">
<!--			<div class="social">
			  <a href="#"><i class="fa fa-facebook"></i></a>
			  <a href="#"><i class="fa fa-twitter"></i></a>
			  <a href="#"><i class="fa fa-google-plus"></i></a>
			  <a href="#"><i class="fa fa-behance"></i></a>
			</div>-->
		  </div>

		  <div class="col-sm-4 right-align">
			<a href="" class="scroll-to-top"><i class="fa fa-angle-up"></i></a>
		  </div>
		</div>
	  </div>
	</footer><!-- .site-footer -->
  </section><!-- .section -->
</div><!-- .main -->

@include('templates.promogear.js')

</body>
</html>
