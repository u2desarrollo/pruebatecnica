<!-- Favicon -->
<link rel="shortcut icon" href="{{ asset('templates/promogear/img/favicon.ico') }}">

<!-- Fonts -->
<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic'>

<!-- Plugins CSS -->
<link rel="stylesheet" href="{{ asset('templates/promogear/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('templates/promogear/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('templates/promogear/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('templates/promogear/css/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('templates/promogear/css/jquery.fullPage.css') }}">
<link rel="stylesheet" href="{{ asset('templates/promogear/css/morris.css') }}">
<link rel="stylesheet" href="{{ asset('templates/promogear/css/jquery.jqplot.css') }}">
<link rel="stylesheet" href="{{ asset('templates/promogear/css/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('templates/promogear/css/revolution/settings.css') }}">
<link rel="stylesheet" href="{{ asset('templates/promogear/css/datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('templates/promogear/css/jquery.mCustomScrollbar.min.css') }}">

<!-- Theme CSS -->
<link rel="stylesheet" href="{{ asset('templates/promogear/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('templates/promogear/css/theme-styles.css') }}">
<style type="text/css">
    #fp-nav ul:hover .fp-tooltip {
        /*right: 20px;*/
        transition: opacity 0.2s ease-in;
        width: auto;
        opacity: 1;
    }
</style>