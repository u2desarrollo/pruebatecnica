<!--[if (!IE)|(gt IE 8)]><!-->
  <script src="{{ asset('templates/promogear/js/jquery-2.1.4.min.js') }}"></script>
<!--<![endif]-->

<!--[if lte IE 8]>
  <script src="{{ asset('templates/promogear/js/jquery-1.11.0.min.js') }}"></script>
<![endif]-->
<script src="{{ asset('templates/promogear/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/jquery.appear.js') }}"></script>
<script src="{{ asset('templates/promogear/js/jquery.easings.min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/jquery.carouFredSel-6.2.1-packed.js') }}"></script>
<script src="{{ asset('templates/promogear/js/jquery.touchwipe.min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/jquery.themepunch.tools.min.js') }}"></script>   
<script src="{{ asset('templates/promogear/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/jquery.mCustomScrollbar.min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/jquery.mlens-1.5.min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/jquery.jqplot.min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/jqplot.donutRenderer.min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/raphael-min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/morris.min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/easy-circle-skill.js') }}"></script>
<script src="{{ asset('templates/promogear/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('templates/promogear/js/TimeCircles.js') }}"></script>
<script src="{{ asset('templates/promogear/js/jquery.fullPage.min.js') }}"></script>
<script src="http://cdn.getshar.es/lib/0.8.0.min.js"></script>
<script src="{{ asset('templates/promogear/js/included-plagins.js') }}"></script>
<script src="{{ asset('templates/promogear/js/main.js') }}"></script>