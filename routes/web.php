<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::post('enviar-email', function (Illuminate\Http\Request $request) {
    Illuminate\Support\Facades\Mail::send(new App\Mail\Contacto($request));
    
    return redirect()->to('/#contacto')->with(array('mensaje' => 'Gracias. Hemos recibido su mensaje con éxito. Nos estaremos contactando con usted lo más pronto posible.'));
})->name('enviar-email');
